<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'ticket';

    public function type()
    {
        return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
    }

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function lot()
    {
        return $this->hasOne('App\TicketLot', 'id', 'ticket_lot_id');
    }

    public function status()
    {
        return $this->hasOne('App\TicketStatus', 'id', 'ticket_status_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
