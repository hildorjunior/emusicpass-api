<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Carbon\Carbon;

class EventController extends Controller
{
    public function getEvents()
    {
        return Event::get();
    }

    public function getEventsSelect()
    {
        return Event::select('id as value', 'name as label')->get();
    }

    public function getEvent($id)
    {
        return Event::find($id);
    }

    public function createEvent()
    {
        $event = new Event;
        $event->name = 'Novo Evento';
        $event->start_date = Carbon::today();
        $event->end_date = Carbon::today();
        $event->save();
        return $event->id;
    }

    public function updateEvent(Request $req, $id)
    {
        $event = Event::find($id);
        $event->name = $req->name;
        $event->start_date = $req->start_date;
        $event->end_date = $req->end_date;
        $event->address = $req->address;
        $event->save();
    }

    public function deleteEvent($id)
    {
        $event = Event::find($id);

        $tickets = Ticket::where('event_id', $event->id)->get();
        foreach ($tickets as $tk) {
          $tk->delete();
        }

        $event->delete();
    }
}
