<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('qr_code')->unique()->nullable();
            $table->integer('ticket_type_id')->unsigned();
            $table->integer('ticket_lot_id')->unsigned();
            $table->integer('ticket_status_id')->unsigned();
            $table->integer('client_id')->unsigned()->nullable();
            $table->string('gate')->nullable();
            $table->dateTime('activated_at')->nullable();
            $table->timestamps();

            $table->foreign('event_id')
                ->references('id')->on('event')
                ->onDelete('cascade');
            $table->foreign('ticket_type_id')
                ->references('id')->on('ticket_type');
            $table->foreign('ticket_lot_id')
                ->references('id')->on('ticket_lot');
            $table->foreign('ticket_status_id')
                ->references('id')->on('ticket_status');
            $table->foreign('client_id')
                ->references('id')->on('client');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket', function (Blueprint $table) {
            $table->dropForeign('ticket_event_id_foreign');
            $table->dropForeign('ticket_ticket_type_id_foreign');
            $table->dropForeign('ticket_ticket_lot_id_foreign');
            $table->dropForeign('ticket_ticket_status_id_foreign');
            $table->dropForeign('ticket_client_id_foreign');
        });
        Schema::drop('ticket');
    }
}
