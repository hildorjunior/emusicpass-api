<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Excel;
use App;
use Log;
use App\Event;
use App\Client;
use App\Ticket;
use App\TicketLot;
use App\TicketType;
use App\TicketStatus;

class TicketController extends Controller
{
    private $event_id;

    public function getTickets(Request $req)
    {
      $tickets = Ticket::with('event')
                       ->with('type')
                       ->with('lot')
                       ->with('status')
                       ->with('client');

      if ($req->event_id) {
        $tickets = $tickets->where('event_id', $req->event_id);
      }
      if ($req->ticket_type_id) {
        $tickets = $tickets->where('ticket_type_id', $req->ticket_type_id);
      }
      if ($req->ticket_lot_id) {
        $tickets = $tickets->where('ticket_lot_id', $req->ticket_lot_id);
      }
      if ($req->ticket_status_id) {
        $tickets = $tickets->where('ticket_status_id', $req->ticket_status_id);
      }

      return $tickets->orderBy('id', 'desc')->get();
    }

    public function activateTickets(Request $req)
    {
      foreach ($req->tickets as $ticket) {
        $tk = Ticket::where('id', $ticket['id'])->first();
        $tk->ticket_status_id = 2;
        $tk->gate = 0;
        $tk->activated_at = Carbon::now()->toDateTimeString();
        $tk->save();
      }
    }

    public function cancelTickets(Request $req)
    {
      foreach ($req->tickets as $ticket) {
        $tk = Ticket::where('id', $ticket['id'])->first();
        $tk->ticket_status_id = 3;
        $tk->save();
      }
    }

    public function importTicketsFromDoc(Request $req, $event_id)
    {
      $this->event_id = $event_id;
      Excel::load($req->file('import_file'), function($reader) {
        $reader->each(function($line) {
          $already_in = Ticket::where('event_id', $this->event_id)
                              ->where('qr_code', $line->qr_code)
                              ->where('ticket_status_id', '<>', 3)
                              ->first();
          if (!$already_in) {
            $cpf = str_replace(['-', '.'], '', $line->client_cpf);

            $new_ticket = new Ticket;
            $new_ticket->event_id = $this->event_id;
            $new_ticket->qr_code = $line->qr_code;
            $new_ticket->ticket_type_id = $line->type_id;
            $new_ticket->ticket_lot_id = $line->lot_id;
            $new_ticket->ticket_status_id = 1;

            $client_exists = Client::where('cpf', $cpf)->first();
            if (!$client_exists) {
              $new_client = new Client;
              $new_client->name = $line->client_name;
              $new_client->cpf = $line->client_cpf;
              if ($line->client_gender != null && $line->client_gender != 'null') {
                $new_client->gender = $line->client_gender;
              }
              if ($line->client_birthday != null && $line->client_birthday != 'null') {
                $new_client->birthday = Carbon::createFromFormat('d/m/Y', $line->client_birthday)->toDateTimeString();
              }
              $new_client->save();
              $new_ticket->client_id = $new_client->id;
            } else {
              $new_ticket->client_id = $client_exists->id;
            }
            $new_ticket->save();
          }
        });
      });
    }

    public function getTicketTypeSelect()
    {
      return TicketType::select('id as value', 'name as label')->get();
    }

    public function newTicketType(Request $req)
    {
      $name = $req->name;
      TicketType::insert(['name' => $name]);
    }

    public function getTicketLotSelect()
    {
        return TicketLot::select('id as value', 'name as label')->get();
    }

    public function getTicketStatusSelect()
    {
        return TicketStatus::select('id as value', 'name as label')->get();
    }
}
