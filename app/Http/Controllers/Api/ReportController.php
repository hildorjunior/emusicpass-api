<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Ticket;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function getReport($event_id)
    {
        $total = Ticket::where('event_id', $event_id)->count();

        $by_type = Ticket::where('event_id', $event_id)
                         ->with('type')
                         ->selectRaw('ticket_type_id, count(*) as total')
                         ->groupBy('ticket_type_id')
                         ->get();

        $by_lot = Ticket::where('event_id', $event_id)
                          ->with('lot')
                          ->selectRaw('ticket_lot_id, count(*) as total')
                          ->groupBy('ticket_lot_id')
                          ->get();

         $by_status = Ticket::where('event_id', $event_id)
                           ->with('status')
                           ->selectRaw('ticket_status_id, count(*) as total')
                           ->groupBy('ticket_status_id')
                           ->get();

        return [
          'total' => $total,
          'by_type' => $by_type,
          'by_lot' => $by_lot,
          'by_status' => $by_status,
        ];
    }
}
