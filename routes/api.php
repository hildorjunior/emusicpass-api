<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('api')->group(function () {
    //Selects
    Route::get('/evento-select', 'EventController@getEventsSelect');
    Route::get('/ingresso-lote-select', 'TicketController@getTicketLotSelect');
    Route::get('/ingresso-tipo-select', 'TicketController@getTicketTypeSelect');
    Route::get('/ingresso-status-select', 'TicketController@getTicketStatusSelect');
    //Eventos
    Route::get('/evento', 'EventController@getEvents');
    Route::get('/evento/{id}', 'EventController@getEvent');
    Route::post('/evento', 'EventController@createEvent');
    Route::post('/evento/{id}', 'EventController@updateEvent');
    Route::delete('/evento/{id}', 'EventController@deleteEvent');
    //Ingressos
    Route::get('/ingresso', 'TicketController@getTickets');
    Route::post('/importar-ingressos/{event_id}', 'TicketController@importTicketsFromDoc');
    Route::post('/ingresso-tipo', 'TicketController@newTicketType');
    Route::post('/ativar-ingressos', 'TicketController@activateTickets');
    Route::post('/cancelar-ingressos', 'TicketController@cancelTickets');
    //Clientes
    Route::get('/cliente', 'ClientController@getClients');
    //Relatorios
    Route::get('/relatorio/{event_id}', 'ReportController@getReport');
});
