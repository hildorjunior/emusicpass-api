<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Carbon\Carbon;

class ClientController extends Controller
{
    public function getClients()
    {
        return Client::get();
    }
}
