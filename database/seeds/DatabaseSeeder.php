<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          ['name'=>'Pista Online'],
          ['name'=>'VIP Online'],
          ['name'=>'Camarote Online'],
      ];
      DB::table('ticket_type')->insert($data);

      $data = [
          ['name'=>'Primeiro Lote'],
          ['name'=>'Segunto Lote'],
          ['name'=>'Terceiro Lote'],
          ['name'=>'Promo'],
          ['name'=>'Único'],
      ];
      DB::table('ticket_lot')->insert($data);

      $data = [
          ['name'=>'Pendente'],
          ['name'=>'Ativado'],
          ['name'=>'Cancelado'],
      ];
      DB::table('ticket_status')->insert($data);
    }
}
