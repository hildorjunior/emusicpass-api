<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketLot extends Model
{
    protected $table = 'ticket_lot';
}
